rm(list = ls())
library(vcd)
library(dplyr)
library(lme4)
library(glmmTMB)
options(scipen = 999)

dir <- "C:/Users/jg344/Desktop/covid project/out2/daily.p1"
setwd(dir)
load("C:/Users/jg344/Desktop/covid project/new/out/data.RData") 
m.result <- readRDS("case.P1.f.m2.rds")

m.zinb  <- glmmTMB(case_daily ~ acs + com + seq.d + acs*seq.d +
          scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
          offset(log(pop_denom)) + (seq.d|modzcta), data = daily.p1,
        family = nbinom2, ziformula  = ~ acs + com )

m.zinb <- glmmTMB(case_daily ~ acs + com + seq.d + acs*seq.d +
                    scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                    offset(log(pop_denom)) + (seq.d|modzcta), data = daily.p1,
                  family = nbinom2, ziformula  = ~ seq.d )


# m.zip <-  glmmTMB(case_daily ~ acs + com + seq.d + acs*seq.d + com*seq.d +
#                     scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
#                     offset(log(pop_denom)) + (seq.d|modzcta), data = daily.p1,
#                   family = poisson, ziformula  = ~ 1)


fit.case <- predict(m.zinb, daily.p1) 
fit.case[fit.case<0] <- 0
obs.case <-  daily.p1$case_daily

rootogram(fit.case, obs.case)

