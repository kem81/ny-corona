rm(list = ls())
options(scipen = 999)


# Data management-------------------------------------------------------------------------
plot <- 'C:/Users/jg344/Desktop/new/'
load("C:/Users/jg344/Desktop/covid project/Junheng/master_data.RData")
library(dplyr)
library(sf)
library(readr)
library(spdep)
library(tmap)
period1 <- master_data %>% 
  filter(!modzcta %in% c(10020, 10110, 10162, 11005, 11040, 11424, 11430)) %>% 
  filter(period == 1) %>% 
  group_by(modzcta) %>% 
  arrange(date) %>% 
  slice(n()) %>% 
  select(modzcta, case_count, pop_denom, death_count, tot_tests, period) %>% 
  mutate(pct_pos = case_count/tot_tests, 
         pos_pc = case_count/pop_denom*1000) 

period2 <-  master_data %>% 
  filter(!modzcta %in% c(10020, 10110, 10162, 11005, 11040, 11424, 11430)) %>% 
  filter(period == 2) %>% 
  group_by(modzcta) %>% 
  arrange(date) %>% 
  slice(n()) %>% 
  select(modzcta, case_count, pop_denom, death_count, tot_tests, period) %>% 
  mutate( # positivity ratio for tests 
          pct_pos = case_count/tot_tests, 
          # incidence 
          pos_pc = case_count/pop_denom*1000,
          # mortality 
         death_pc = death_count/pop_denom*1000)

geo <- read_csv("C:/Users/jg344/Desktop/new/Modified_Zip_Code_Tabulation_Areas__MODZCTA_.csv") %>% 
        rename(modzcta = MODZCTA, geometry = the_geom) %>% 
        select(modzcta, geometry)

s.p1 <- left_join(period1, geo, by = "modzcta")
s.p2 <- left_join(period2, geo, by = "modzcta")
m.p1 <- sf::st_as_sf( s.p1, wkt = "geometry" )
m.p2 <- sf::st_as_sf( s.p2, wkt = "geometry")


# global moran index ------------------------------------------------------

nb.p1 <- poly2nb(m.p1, queen=TRUE);# nb.p2  1 region with no links: 38
lw.p1 <- nb2listw(nb.p1, style="W", zero.policy=TRUE);lw.p1
inc.lag <- lag.listw(lw.p1, m.p1$pct_pos)
plot(inc.lag ~ m.p1$pct_pos, pch=16, asp=1)
M1 <- lm(inc.lag ~ m.p1$pct_pos)
abline(M1, col="blue")
coef(M1)[2]


m..p1 <- m.p1[-38,]
nb <- poly2nb(m..p1, queen=TRUE)
lw <- nb2listw(nb, style="W", zero.policy=TRUE)
# period 1, pct_pos
I <- moran(m..p1$pct_pos, lw, length(nb), Szero(lw))[1]
moran.test(m..p1$pct_pos, lw, alternative="greater")

# period 1, pos_pc
I <- moran(m..p1$pos_pc, lw, length(nb), Szero(lw))[1]
moran.test(m..p1$pos_pc, lw, alternative="greater")


m..p2 <- m.p2[-38,]
nb <- poly2nb(m..p2, queen=TRUE)
lw <- nb2listw(nb, style="W", zero.policy=TRUE)
# period 2, pct_pos
I <- moran(m..p2$pct_pos, lw, length(nb), Szero(lw))[1]
moran.test(m..p2$pct_pos, lw, alternative="greater")

# period 2, pos_pc
I <- moran(m..p2$pos_pc, lw, length(nb), Szero(lw))[1]
moran.test(m..p2$pos_pc, lw, alternative="greater")

# period 2, death_pc
I <- moran(m..p2$death_pc, lw, length(nb), Szero(lw))[1]
moran.test(m..p2$death_pc, lw, alternative="greater")

# local moran index -------------------------------------------------------------------------
m..p1 <- m.p1[-38,]
nb <- poly2nb(m..p1, queen=TRUE)
lw <- nb2listw(nb, style="W", zero.policy=TRUE)

# period 1, pct_pos
local <- localmoran(x = m..p1$pct_pos, listw = nb2listw(nb, style = "W"))

png('p1.pct_pos_lag.png', units = 'in', width = 4, height = 4, res = 300)
moran <- moran.plot(m..p1$pct_pos, listw = nb2listw(nb, style = "W"),
                    xlab = "positivity ratio for tests",
                    ylab = "spatially lagged: positivity ratio for test") 
dev.off()

moran.map <- cbind(m..p1, local)
png('p1.pct_pos_Ii.png', units = 'in', width = 4, height = 4, res = 300)
tm_shape(moran.map) + tm_fill(col = "Ii",
                              style = "quantile",
                              title = "local moran statistic",
                              midpoint = NA) + 
                  tm_layout(frame = FALSE, title = "positivity ratio for tests") 
dev.off()

# period 1, pos_pc
local <- localmoran(x = m..p1$pos_pc, listw = nb2listw(nb, style = "W"))
png('p1.pos_pc_lag.png', units = 'in', width = 4, height = 4, res = 300)
moran <- moran.plot(m..p1$pos_pc, listw = nb2listw(nb, style = "W"),
                    xlab = "incidence",
                    ylab = "spatially lagged: incidence") 
dev.off()

moran.map <- cbind(m..p1, local)
png('p1.pos_pc_Ii.png', units = 'in', width = 4, height = 4, res = 300)
tm_shape(moran.map) + tm_fill(col = "Ii",
                              style = "quantile",
                              title = "local moran statistic",
                              midpoint = NA) + 
                   tm_layout(frame = FALSE, title = 'incidence') 
dev.off()


m..p2 <- m.p2[-38,]
nb <- poly2nb(m..p2, queen=TRUE)
lw <- nb2listw(nb, style="W", zero.policy=TRUE)
# period 2, pct_pos

local <- localmoran(x = m..p2$pct_pos, listw = nb2listw(nb, style = "W"))
png('p2.pct_pos_lag.png', units = 'in', width = 4, height = 4, res = 300)
moran <- moran.plot(m..p2$pct_pos, listw = nb2listw(nb, style = "W"),
                    xlab = "positivity ratio for tests",
                    ylab = "spatially lagged: positivity ratio for test") 
dev.off()

moran.map <- cbind(m..p2, local)
png('p2.pct_pos_Ii.png', units = 'in', width = 4, height = 4, res = 300)
tm_shape(moran.map) + tm_fill(col = "Ii",
                              style = "quantile",
                              title = "local moran statistic",
                              midpoint = NA) + 
                 tm_layout(frame = FALSE, title = 'positivity ratio for tests') 
dev.off()
# period 2, pos_pc
png('p2.pos_pc_lag.png', units = 'in', width = 4, height = 4, res = 300)
local <- localmoran(x = m..p2$pos_pc, listw = nb2listw(nb, style = "W"))
moran <- moran.plot(m..p2$pos_pc, listw = nb2listw(nb, style = "W"),
                    xlab = "incidence",
                    ylab = "spatially lagged: incidence") 
dev.off()

moran.map <- cbind(m..p2, local)
png('p2.pos_pc_Ii.png', units = 'in', width = 4, height = 4, res = 300)
tm_shape(moran.map) + tm_fill(col = "Ii",
                              style = "quantile",
                              title = "local moran statistic",
                              midpoint = NA) + 
       tm_layout(frame = FALSE, title = 'incidence') 
dev.off()

# period 2, death_pc
local <- localmoran(x = m..p2$death_pc, listw = nb2listw(nb, style = "W"))

png('p2.death_pc_lag.png', units = 'in', width = 4, height = 4, res = 300)
moran <- moran.plot(m..p2$death_pc, listw = nb2listw(nb, style = "W"),
                    xlab = "mortality",
                    ylab = "spatially lagged: mortality") 
dev.off()

moran.map <- cbind(m..p2, local)
png('p2.death_pc_Ii.png', units = 'in', width = 4, height = 4, res = 300)
tm_shape(moran.map) + tm_fill(col = "Ii",
                              style = "quantile",
                              title = "local moran statistic",
                              midpoint = NA)+
                 tm_layout(frame = FALSE, title = 'mortality') 
dev.off()

# Plot LISA clusters ------------------------------------------------------
m..p1 <- m.p1[-38,]
nb <- poly2nb(m..p1, queen=TRUE)
lw <- nb2listw(nb, style="W", zero.policy=TRUE)


# period 1, pct_pos
local <- localmoran(x = m..p1$pct_pos, listw = nb2listw(nb, style = "W"))
quadrant <- vector(mode="numeric",length=nrow(local))
m.qualification <- m..p1$pct_pos - mean(m..p1$pct_pos)
m.local <- local[,1] - mean(local[,1])

signif <- 0.1 
quadrant[m.qualification >0 & m.local>0] <- 4  
quadrant[m.qualification <0 & m.local<0] <- 1      
quadrant[m.qualification <0 & m.local>0] <- 2
quadrant[m.qualification >0 & m.local<0] <- 3
quadrant[local[,5]>signif] <- 0   
brks <- c(0,1,2,3,4)
colors <- c("white","blue",rgb(0,0,1,alpha=0.4),rgb(1,0,0,alpha=0.4),"red")

png('p1.pct_pos_clusters.png', units = 'in', width = 7, height = 5, res = 300)
moran.plot <- plot(m..p1['pct_pos'],border="lightgray",
                   col=colors[findInterval(quadrant,brks,all.inside=FALSE)],
                   main = "positivity ratio for tests")
legend("bottomleft", legend = c("insignificant","low-low","low-high","high-low","high-high"),
       fill=colors, bty='n', x = 'left') 
dev.off()

# period 1, pos_pc
local <- localmoran(x = m..p1$pos_pc, listw = nb2listw(nb, style = "W"))
quadrant <- vector(mode="numeric",length=nrow(local))
m.qualification <- m..p1$pos_pc - mean(m..p1$pos_pc)
m.local <- local[,1] - mean(local[,1])

signif <- 0.1 
quadrant[m.qualification >0 & m.local>0] <- 4  
quadrant[m.qualification <0 & m.local<0] <- 1      
quadrant[m.qualification <0 & m.local>0] <- 2
quadrant[m.qualification >0 & m.local<0] <- 3
quadrant[local[,5]>signif] <- 0   
brks <- c(0,1,2,3,4)
colors <- c("white","blue",rgb(0,0,1,alpha=0.4),rgb(1,0,0,alpha=0.4),"red")

png('p1.pos_pc_clusters.png', units = 'in', width = 7, height = 5, res = 300)
moran.plot <- plot(m..p1['pos_pc'],border="lightgray",
                   col=colors[findInterval(quadrant,brks,all.inside=FALSE)],
                   main = "incidence")
legend("bottomleft", legend = c("insignificant","low-low","low-high","high-low","high-high"),
       fill=colors, bty='n', x = 'left') 
dev.off()

# period 2, pct_pos
m..p2 <- m.p2[-38,]
nb <- poly2nb(m..p2, queen=TRUE)
lw <- nb2listw(nb, style="W", zero.policy=TRUE)
# period 2, pct_pos
local <- localmoran(x = m..p2$pct_pos, listw = nb2listw(nb, style = "W"))
quadrant <- vector(mode="numeric",length=nrow(local))
m.qualification <- m..p2$pct_pos - mean(m..p2$pct_pos)
m.local <- local[,1] - mean(local[,1])

signif <- 0.1 
quadrant[m.qualification >0 & m.local>0] <- 4  
quadrant[m.qualification <0 & m.local<0] <- 1      
quadrant[m.qualification <0 & m.local>0] <- 2
quadrant[m.qualification >0 & m.local<0] <- 3
quadrant[local[,5]>signif] <- 0   
brks <- c(0,1,2,3,4)
colors <- c("white","blue",rgb(0,0,1,alpha=0.4),rgb(1,0,0,alpha=0.4),"red")

png('p2.pct_pos_clusters.png', units = 'in', width = 7, height = 5, res = 300)
moran.plot <- plot(m..p2['pct_pos'],border="lightgray",
                   col=colors[findInterval(quadrant,brks,all.inside=FALSE)],
                   main = "positivity ratio for tests")
legend("bottomleft", legend = c("insignificant","low-low","low-high","high-low","high-high"),
       fill=colors, bty='n', x = 'left') 
dev.off()

# period 2, pos_pc
local <- localmoran(x = m..p2$pos_pc, listw = nb2listw(nb, style = "W"))
quadrant <- vector(mode="numeric",length=nrow(local))
m.qualification <- m..p2$pos_pc - mean(m..p2$pos_pc)
m.local <- local[,1] - mean(local[,1])

signif <- 0.1 
quadrant[m.qualification >0 & m.local>0] <- 4  
quadrant[m.qualification <0 & m.local<0] <- 1      
quadrant[m.qualification <0 & m.local>0] <- 2
quadrant[m.qualification >0 & m.local<0] <- 3
quadrant[local[,5]>signif] <- 0   
brks <- c(0,1,2,3,4)
colors <- c("white","blue",rgb(0,0,1,alpha=0.4),rgb(1,0,0,alpha=0.4),"red")

png('p2.pos_pc_clusters.png', units = 'in', width = 7, height = 5, res = 300)
moran.plot <- plot(m..p2['pos_pc'],border="lightgray",
                   col=colors[findInterval(quadrant,brks,all.inside=FALSE)],
                   main = "incidence")
legend("bottomleft", legend = c("insignificant","low-low","low-high","high-low","high-high"),
       fill=colors, bty='n', x = 'left') 
dev.off()

# period 2, death_pc
local <- localmoran(x = m..p2$death_pc, listw = nb2listw(nb, style = "W"))
quadrant <- vector(mode="numeric",length=nrow(local))
m.qualification <- m..p2$death_pc - mean(m..p2$death_pc)
m.local <- local[,1] - mean(local[,1])

signif <- 0.1 
quadrant[m.qualification >0 & m.local>0] <- 4  
quadrant[m.qualification <0 & m.local<0] <- 1      
quadrant[m.qualification <0 & m.local>0] <- 2
quadrant[m.qualification >0 & m.local<0] <- 3
quadrant[local[,5]>signif] <- 0   
brks <- c(0,1,2,3,4)
colors <- c("white","blue",rgb(0,0,1,alpha=0.4),rgb(1,0,0,alpha=0.4),"red")

png('p2.death_pc_clusters.png', units = 'in', width = 7, height = 5, res = 300)
moran.plot <- plot(m..p2['death_pc'],border="lightgray",
                   col=colors[findInterval(quadrant,brks,all.inside=FALSE)],
                   main = "mortality")
legend("bottomleft", legend = c("insignificant","low-low","low-high","high-low","high-high"),
       fill=colors, bty='n', x = 'left') 
dev.off()
