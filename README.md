
# ny-corona

Analysis of NYC Coronavirus data by modzcta.

## data_download_acs.Rmd

This file downloads the 2019 5-year ACS data for NYC. We use codes located in aux/acs_2019_codes.csv. 

## data_download_acs_us.Rmd

This file downloads the 2019 5-year ACS data for US. We use codes located in aux/acs_2019_codes.csv. 

## acs_com_data_preprocess.Rmd

This file reads in the CDC 500 Cities Comorbidities dataset and the ACS dataset for NYC, and converts everything to modzcta level. Creates all the ACS variables we are interested in (proportions of people/households with the variable in each modzcta) and joins the data together to output acs_com_data.csv. 

## analysis_lcm_com_3.Rmd

This script performs the Mplus latent class modeling for the comorbidities variables. The model that performs best is the one with 3 classes: high, medium, and low. Reads in mplus input files from aux folder, and outputs .out files also to .aux folder. 

## analysis_lcm_ses_2.Rmd

This script performs the Mplus latent class modeling for the SES variables. The model that performs best is the one with 2 classes for Advantage and 2 classes for Disadvantage. 

## covid_data_prep.Rmd

Downloads COVID-19 data in NYC from github: https://github.com/nychealth/coronavirus-data 

Covid-19 related variables are: modzcta, neighborhood, borough, case count, case rate, population denominator, death count, death rate, percent positive, total tests, timestamp, and date (which is just the timestamp variable converted into a date using lubridate). 

Reads in the ACS and COM dataset (acs_com_data.csv), as well as latent class results for both ACS (output/nyc_ses_2_2_cprobs_plot.csv) and COM (output/nyc_com_3_cprobs_new.csv), and merges everything together to form master dataset (master_data.csv). 

## acs_com_vis.Rmd

Brings in covid data, acs data, comorbidities data, and maps the quantiles across modzctas in NYC. Outputs maps to output folder (maps). 




