---
title: "data_download_acs"
output: html_document
---

```{r echo = F, message = F}
library(RColorBrewer)
library(tidycensus)
library(tidyverse)
library(tidyselect)
library(tidyr)
library(ggplot2)
library(viridis)
library(acs)
library(sf)
options(tigris_use_cache = TRUE)
```

### Get Codes from ACS API

Downloading these codes to R allows you to search and find variables of interest. This step only needs to be done once. The codes selected for this analysis are saved in the file, acs_2014_codes.csv.

```{r}
# only run the first time
#census_api_key("f6b39aac448c33c38c1e90e5d2a085d58726c23b", install=TRUE, overwrite=TRUE)
#readRenviron("~/.Renviron")
#Sys.getenv("CENSUS_API_KEY")

# look-up table for the ACS data
acs_vrbs <- load_variables(2019, "acs5", cache = TRUE)

acs_vrbs[(which(acs_vrbs$name == "C24040_045")-20):(which(acs_vrbs$name == "C24040_045")+30),]

```

Codes from the above chunk are selected and saved for variables of interest. Codes available in "aux/acs_2014_codes_updated_internet.csv". 

### Download ACS variables of interest

Here, we upload the codes selected and saved from the previous step, and download them from the 2017 5-year ACS. 

```{r}
my_codes <- read_csv(file = "misc/acs_2019_codes.csv")

state_estimates_geo <- get_acs(
  geography = "tract",
  state = "NY",
  county = c("Queens", "New York", "Bronx", "Kings", "Richmond"),
  variables = na.omit(my_codes$acs_code),
  survey = "acs5",
  year = 2019,
  output = "tidy",
  geometry = T
) 
```

## Reformat the ACS data

```{r}
# change from long to wide
acs_df <- state_estimates_geo %>%
  dplyr::select(-moe) %>%
  spread(variable, estimate)

# change from codes to labels
x <- names(acs_df)
for (i in 1:length(names(acs_df))) {
  if (x[i] %in% c("GEOID", "NAME", "geometry")) {
    x[i] <- x[i]
  } else {
    x[i] <- my_codes$my_vrb[my_codes$acs_code == x[i]]
  }
}
names(acs_df) <- x
```

### Variable modifications - Crowded housing and phone service.
 
In the ACS database, the number of occupants per room is partitioned into three cutpoints (1.01-1.5, 1.51-2, 2.1 and up) and house type (rental, owned). We want one variable to denote crowded housing (defined as >1 person per room) and we are uninterested in rental vs. owned, so we sum to get the number of all houses (rental or owned) with more than 1 occpupant per room and add this to our acs data frame.

```{r}
num_occupants_1.01_up_all <- acs_df %>%
  dplyr::select(num_occupants_1.01_1.5_owned, num_occupants_1.51_2_owned,
         num_occupants_2.1_up_owned, num_occupants_1.01_1.5_rented,
         num_occupants_1.51_2_rented, num_occupants_2.1_up_rented) %>%
  st_drop_geometry() %>%
  rowSums()
acs_df <- acs_df %>%
  mutate(num_occupants_crowded = num_occupants_1.01_up_all)
```

For phone service, we are uninterested in rental versus owned housing, so we sum to get the number of all houses (rental or owned) with phone service and add this to our acs data frame.

```{r}
phone_service_rented_owned <- acs_df %>%
  dplyr::select(phone_service_owned, phone_service_rented) %>%
  st_drop_geometry() %>%
  rowSums()

acs_df <- acs_df %>%
  mutate(phone_service = phone_service_rented_owned)
```


### Output ACS data for the analysis in a .csv 

```{r}
# remove the geometry
acs_df_out <- acs_df %>%
  st_drop_geometry()

write.csv(acs_df_out, "output/acs_2019_5yr.csv")

# stop here and then transition to "the_city_data.Rmd"
```
