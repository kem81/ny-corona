rm(list = ls())

library(glmmTMB)
library(dplyr)
options(scipen = 999)
setwd("C:/Users/jg344/Desktop/covid project/Junheng/R out 3/")
load("C:/Users/jg344/Desktop/covid project/Junheng/R out 3/data.RData")

setwd("C:/Users/jg344/Desktop/covid project/Junheng/inla/")

weekly.p1 <- readRDS("weekly.p1.rds") %>%  rename(acs = acs_class_name)
weekly.p2 <- readRDS('weekly.p2.rds') %>%  rename(acs = acs_class_name)

# Daily period 1
# Case, full model 2, 7-day average, zero inflated
case.P1 <- glmmTMB(case.7dayavg ~ acs + com + seq.d + acs*seq.d + com*seq.d +
                              scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                              offset(log(pop_denom)) + (seq.d|modzcta), data = daily.p1,
                            family = nbinom2, ziformula  = ~ 1)
saveRDS(case.P1, "case.P1.d.rds")

# Case, full model 2, 7-day average, zero inflated
case.P2 <- glmmTMB(case.7dayavg ~ acs + com + seq.d + acs*seq.d + com*seq.d +
                     scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                     offset(log(pop_denom)) + (seq.d|modzcta), data = daily.p2,
                   family = nbinom2, ziformula  = ~ 1)
saveRDS(case.P2, "case.P2.d.rds")

# Death, full model 2, 7-day average zero inflated
death.P2 <- glmmTMB(death.7dayavg ~ acs + com + seq.d + 
                     scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                     offset(log(pop_denom)) + (seq.d|modzcta), data = daily.p2,
                   family = nbinom2, ziformula = ~ 1)
saveRDS(death.P2, "death.P2.d.rds")

# Case, full model 2, NB
case.P1.w <- glmmTMB(case_weekly1 ~ acs + com + seq.w +
                     scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                     offset(log(pop_denom)) + (seq.w|modzcta), data = weekly.p1,
                   family = nbinom2)
saveRDS(case.P1.w, "case.P1.w.rds")

# Case, full model 2, NB
case.P2.w <- glmmTMB(case_weekly1 ~ acs + com + seq.w + 
                     scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                     offset(log(pop_denom)) + (seq.w|modzcta), data = weekly.p2,
                   family = nbinom2)
saveRDS(case.P2.w, "case.P2.w.rds")

# Death, full model 2, zi
death.P2.w <- glmmTMB(death_weekly1 ~ acs + com + seq.w + 
                     scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                     offset(log(pop_denom)) + (1|modzcta), data = weekly.p2,
                   family = nbinom2,  ziformula  = ~ 1)
saveRDS(death.P2.w, "death.P2.w.rds")

setwd('C:/Users/jg344/Desktop/covid project/Junheng/R out 3/out2/')
saveRDS(case.P1.w, "case.P1.w.rds")
saveRDS(case.P2.w, "case.P2.w.rds")
saveRDS(death.P2.w, "death.P2.w.rds")
