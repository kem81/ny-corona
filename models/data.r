rm(list = ls())

library(dplyr)
# library(lme4)
# library(glmmTMB)
library(readxl)
library(here)
options(scipen = 999)

setwd(here::here('models'))

# master_data <- read_excel("master_data.xlsx")
# master_data_week <- read_excel("master_data_week.xlsx")
# save(master_data, master_data_week, file = "data.RData")

load("data.RData")

# saveRDS(master_data, file = "master_data.rds")
# readRDS(file = "master_data.rds")
# saveRDS(master_data_week, file = "master_data_week.rds")
# readRDS(file = "master_data_week.rds")

# Per Day-------------------------------------------------------------------------

# response variable: case_daily, case_7dayavg
# death_daily, death_7dayavg
daily <- master_data %>% arrange(modzcta, date) %>% 
  dplyr::select(modzcta, pop_denom, date, case_count, case_daily, case_7dayavg, period,
                death_count, death_daily, death_7dayavg,
                com_class, acs_class, acs_class_name, 
                male_prop,
                age_over_65_prop,
                pop_non_hispanic_black_prop,
                hispanic_prop) %>% group_by(modzcta, period) %>% 
          mutate(seq.d = seq_along(date)) %>% ungroup() %>% 
          mutate(case.7dayavg = round(as.numeric(case_7dayavg)),
                 death.7dayavg = round(as.numeric(death_7dayavg)))%>% 
         filter(!modzcta %in% c(10020, 10110, 10162, 11005, 11040, 11424, 11430)) %>% 
          mutate(acs = factor(acs_class,
                             levels = c("1 (1, 1)", "2 (1, 2)", "3 (2, 1)", "4 (2, 2)"),
                             labels = c("Low Dis. High Adv.",
                                        "Low Dis. Low Adv.",
                                        "High Dis. High Adv.",
                                        "High Dis. Low Adv.")),
                com = factor(com_class, levels = c("3 (Low)","2 (Medium)","1 (High)")))

dim(daily)

daily.p1 <- daily %>% filter(period == 1) %>% arrange(modzcta,date)
daily.p2 <- daily %>% filter(period == 2) %>% arrange(modzcta,date)

# Check Daily data-------------------------------------------------------------------------

# Daily.p1 data set: 177 modzcta, 63 consecutive days, 9 weeks: from 2020-04-03 to 2020-06-08

summary(daily.p1);summary(daily.p2)
# Q1 death related variable are logical variable. after change, correct

length(unique(daily.p1$modzcta)); #184 modzcta,  after change 177 
data.frame(table(daily.p1$modzcta, useNA = "always")) %>% dplyr::filter(Freq ==1) 
# Q2 7 modzcta has only 1 observation: 10020, 10110, 10162, 11005, 11040, 11424, 11430, all other are NAs, remove?   
length(unique(daily.p1$date));# 63 days
data.frame(table(daily.p1$date, useNA = "always"))
table(daily.p1$date, daily.p1$seq.d)
aa <- daily.p1 %>% dplyr::filter(date == "2020-04-10" & modzcta ==11697)
# Q3 11697 has two records for date 2020-04-10, one case record is 9, one case record is 0, which one is valid? 
# after change, correct.

# Daily.p2 data set: 177 modzcta, 179 days: from 2020-11-10 to 2021-05-10
length(unique(daily.p2$modzcta)) # 177 modzcta
length(unique(daily.p2$date)) # 179 days: from 2020-11-10 to 2021-05-10
data.frame(table(daily.p2$date)) %>% summarise(table(Freq))
table(daily.p2$date, daily.p2$seq.d, useNA = "always")
table(daily.p2$seq.d, useNA = "always")

# Per Week-------------------------------------------------------------------------

# method1: calculating daily totals and then summing them over the week
# method2: subtracting 1st day of the week cumulative totals from the 7th day

weekly <- master_data_week %>% arrange(modzcta, week) %>% 
  dplyr::select(modzcta,pop_denom, week, case_weekly1, case_weekly2, period,
                death_weekly1, death_weekly2, com_class, acs_class, acs_class_name,
                male_prop,
                age_over_65_prop,
                pop_non_hispanic_black_prop,
                hispanic_prop) %>% group_by(modzcta, period) %>%
                mutate(seq.w = seq_along(week)) %>% ungroup() %>% 
          mutate(acs = factor(acs_class, 
                              levels = c("1 (1, 1)", "2 (1, 2)", "3 (2, 1)", "4 (2, 2)"),
                              labels = c("Low Dis. High Adv.",
                                         "Low Dis. Low Adv.",
                                         "High Dis. High Adv.",
                                         "High Dis. Low Adv.")),
                com = factor(com_class, levels = c("3 (Low)", "2 (Medium)", "1 (High)")))

weekly.p1 <- weekly %>% filter(period == 1) %>% arrange(modzcta, week)
weekly.p2 <- weekly %>% filter(period == 2) %>% arrange(modzcta, week)


# Check Weekly data -------------------------------------------------------------------------


summary(weekly.p1);summary(weekly.p2)
# Q1 death related variable are logical variable. after change correct. 

# Weekly.p1 data set: 177 modzcta, 6 weeks: 2020-04-08, 2020-04-15, 2020-04-22, 2020-04-29, 2020-05-06, 2020-05-27

length(unique(weekly.p1$modzcta)) # 177 modzcta
length(unique(weekly.p1$week)) # 6 weeks
data.frame(table(weekly.p1$week, useNA = "always"))
# Q1: 6 weeks: 2020-04-08, 2020-04-15, 2020-04-22, 2020-04-29, 2020-05-06, 2020-05-27, not consecutive?
# should be 9 weeks, right, data is not missing in the daily data set.
# after change, incomplete weeks are trimmed 
table(weekly.p1$week, weekly.p1$seq.w, useNA = "always")
aa <- weekly.p1 %>% filter(modzcta == 11697)
# Q2 modzcta 11697 only has 5 records, missing the first one,start from 2020-04-22 to 2020-05-27.
# after change, fixed. 
# Weekly.p2 data set: 
length(unique(weekly.p2$modzcta)) # 177 modzcta
length(unique(weekly.p2$week)) # 24 weeks, one NA
data.frame(table(weekly.p2$week, useNA = "always"))
# Q3 missing one week?
# 2020-11-11, 2020-11-18, 2020-11-25, 2020-12-02, 2020-12-09, 2020-12-16, 2020-12-23  
# 2021-01-01, 2021-01-08, 2021-01-15, 2021-01-22, 2021-01-29, 2021-02-05, 2021-02-12  
# 2021-02-19, 2021-02-26, 2021-03-05, 2021-03-12, (missing one week) 2021-03-26, 2021-04-02, 2021-04-09  
# 2021-04-16, 2021-04-23, 2021-04-30  

aa <- weekly.p2 %>% dplyr::filter(is.na(week))
# 10305, 11211 have two records with missing week. 
table(weekly.p2$week, weekly.p2$seq.w, useNA = "always")

# Save data-------------------------------------------------------------------------

save(daily, daily.p1, daily.p2, weekly, weekly.p1, weekly.p2, file = "data.RData")
save(daily, daily.p1, daily.p2, file = "data.RData")
