rm(list = ls())

library(glmmTMB)
library(here)
options(scipen = 999)

setwd(here::here('models'))

load("data.RData")

# Daily.p1 -------------------------------------------------------------------------

# response variables: case_daily, case.7dayavg
# primary covariates of interest: acs, com
# covariates: male_prop ,age_over_65_prop, pop_non_hispanic_black_prop, hispanic_prop
# offset: pop_denom

# negative binomial model: 

case.D.p1.acs <- glmmTMB( case_daily ~ acs +
                             scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                             offset(log(pop_denom)) + (1|modzcta), data = daily.p1,
                            family = nbinom2)
summary(case.D.p1.acs)

saveRDS(case.D.p1.acs, "daily.p1/case.D.p1.acs.rds")
case.D.p1.com <- glmmTMB( case_daily ~ com +
                             scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                             offset(log(pop_denom)) + (1|modzcta), data = daily.p1,
                            family = nbinom2)
saveRDS(case.D.p1.com, "daily.p1/case.D.p1.com.rds")
case.D.p1.acs.avg <- glmmTMB( case.7dayavg ~ acs +
                                 scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                                 offset(log(pop_denom)) + (1|modzcta), data = daily.p1,
                              family = nbinom2)
saveRDS(case.D.p1.acs.avg, "daily.p1/case.D.p1.acs.avg.rds")
case.D.p1.com.avg <- glmmTMB( case.7dayavg ~ com +
                                 scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                                 offset(log(pop_denom)) + (1|modzcta), data = daily.p1,
                              family = nbinom2)
saveRDS(case.D.p1.com.avg, "daily.p1/case.D.p1.com.avg.rds")

# Daily.p2 -------------------------------------------------------------------------

# response variable: case_daily, case.7dayavg, 
#                    death_daily, death.7dayavg
# primary covariates of interest: acs, com
# covariates: male_prop ,age_over_65_prop, pop_non_hispanic_black_prop, hispanic_prop
# offset: pop_denom

# case
case.D.p2.acs <- glmmTMB( case_daily ~ acs +
                             scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                             offset(log(pop_denom)) + (1|modzcta), data = daily.p2,
                          family = nbinom2)
saveRDS(case.D.p2.acs, "daily.p2/case.D.p2.acs.rds")
case.D.p2.com <- glmmTMB( case_daily ~ com +
                             scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                             offset(log(pop_denom)) + (1|modzcta), data = daily.p2,
                          family = nbinom2)
saveRDS(case.D.p2.com, "daily.p2/case.D.p2.com.rds")
# case average
case.D.p2.acs.avg <- glmmTMB( case.7dayavg ~ acs +
                                 scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                                 offset(log(pop_denom)) + (1|modzcta), data = daily.p2,
                            family = nbinom2)
saveRDS(case.D.p2.acs.avg, "daily.p2/case.D.p2.acs.avg.rds")
case.D.p2.com.avg <- glmmTMB( case.7dayavg ~ com +
                                 scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                                 offset(log(pop_denom)) + (1|modzcta), data = daily.p2,
                            family = nbinom2)
saveRDS(case.D.p2.com.avg, "daily.p2/case.D.p2.com.avg.rds")

#death 
death.D.p2.acs <- glmmTMB(death_daily ~ acs + 
                            scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                            offset(log(pop_denom)) + (1|modzcta), data = daily.p2,
                          family = poisson, ziformula  = ~ 1)

saveRDS(death.D.p2.acs,"daily.p2/death.D.p2.acs.rds")
death.D.p2.com <- glmmTMB(death_daily ~ com + 
                            scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                            offset(log(pop_denom)) + (1|modzcta), data = daily.p2,
                          family = poisson, ziformula  = ~ 1)
saveRDS(death.D.p2.com,"daily.p2/death.D.p2.com.rds")
death.D.p2.acs.avg <- glmmTMB(death.7dayavg ~ acs + 
                                scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                                offset(log(pop_denom)) + (1|modzcta), data = daily.p2,
                              family = poisson, ziformula  = ~ 1)
# Warning message:
# In fitTMB(TMBStruc) : Model convergence problem; false convergence (8). See vignette('troubleshooting')
saveRDS(death.D.p2.acs.avg,"daily.p2/death.D.p2.acs.avg.rds")
death.D.p2.com.avg <- glmmTMB(death.7dayavg ~ com + 
                                scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                                offset(log(pop_denom)) + (1|modzcta), data = daily.p2,
                              family = poisson, ziformula  = ~ 1)
# Warning messages:
#   1: In fitTMB(TMBStruc) :
#   Model convergence problem; non-positive-definite Hessian matrix. See vignette('troubleshooting')
# 2: In fitTMB(TMBStruc) :
#   Model convergence problem; false convergence (8). See vignette('troubleshooting')
saveRDS(death.D.p2.com.avg,"daily.p2/death.D.p2.com.avg.rds")


# Weekly.p1 -------------------------------------------------------------------------

# response variable: case_weekly1, case_weekly2
# primary covariates of interest: acs, com
# covariates: male_prop ,age_over_65_prop, pop_non_hispanic_black_prop, hispanic_prop
# offset: pop_denom

case.W.p1.acs <- glmmTMB(case_weekly1 ~ acs +
                            scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                            offset(log(pop_denom)) + (1|modzcta), data = weekly.p1,
                          family = nbinom2)
# boundary (singular) fit: see ?isSingular
# 
# Warning message:
#   In checkConv(attr(opt, "derivs"), opt$par, ctrl = control$checkConv,  :
#                  Model failed to converge with max|grad| = 0.00335143 (tol = 0.002, component 1)
saveRDS(case.W.p1.acs,"weekly.p1/case.W.p1.acs.rds")
case.W.p1.com <- glmmTMB(case_weekly1 ~ com +
                            scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                            offset(log(pop_denom)) + (1|modzcta), data = weekly.p1,
                          family = nbinom2)
# boundary (singular) fit: see ?isSingular

saveRDS(case.W.p1.com,"weekly.p1/case.W.p1.com.rds")
case.W.p1.acs.2 <- glmmTMB(case_weekly2 ~ acs +
                              scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                              offset(log(pop_denom)) + (1|modzcta), data = weekly.p1,
                            family = nbinom2)
# boundary (singular) fit: see ?isSingular
saveRDS(case.W.p1.acs.2,"weekly.p1/case.W.p1.acs.2.rds")
case.W.p1.com.2 <- glmmTMB(case_weekly2 ~ com +
                              scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                              offset(log(pop_denom)) + (1|modzcta), data = weekly.p1,
                            family = nbinom2)
# boundary (singular) fit: see ?isSingular
saveRDS(case.W.p1.com.2,"weekly.p1/case.W.p1.com.2.rds")


# Weekly.p2 ---------------------------------------------------------------

# response variable: case_weekly1, case_weekly2
#                    death_weekly1, death_weekly2
# primary covariates of interest: acs, com
# covariates: male_prop ,age_over_65_prop, pop_non_hispanic_black_prop, hispanic_prop
# offset: pop_denom

case.W.p2.acs <- glmmTMB(case_weekly1 ~ acs +
                            scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                            offset(log(pop_denom)) + (1|modzcta), data = weekly.p2,
                          family = nbinom2)
saveRDS(case.W.p2.acs, "weekly.p2/case.W.p2.acs.rds")
case.W.p2.com <- glmmTMB(case_weekly1 ~ com + 
                            scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                            offset(log(pop_denom)) + (1|modzcta), data = weekly.p2,
                         family = nbinom2)
# Warning message:
#   In checkConv(attr(opt, "derivs"), opt$par, ctrl = control$checkConv,  :
#                  Model failed to converge with max|grad| = 0.00219587 (tol = 0.002, component 1)
saveRDS(case.W.p2.com, "weekly.p2/case.W.p2.com.rds")
case.W.p2.acs.2 <- glmmTMB(case_weekly2 ~ acs +
                              scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                              offset(log(pop_denom)) + (1|modzcta), data = weekly.p2,
                           family = nbinom2)
saveRDS(case.W.p2.acs.2, "weekly.p2/case.W.p2.acs.2.rds")
case.W.p2.com.2 <- glmmTMB(case_weekly2 ~ com +
                              scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                              offset(log(pop_denom)) + (1|modzcta), data = weekly.p2,
                           family = nbinom2)
saveRDS(case.W.p2.com.2, "weekly.p2/case.W.p2.com.2.rds")

death.W.p2.acs <- glmmTMB(death_weekly1 ~ acs + 
                            scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                            offset(log(pop_denom)) + (1|modzcta), data = weekly.p2,
                          family = poisson, ziformula  = ~ 1)
saveRDS(death.W.p2.acs, "weekly.p2/death.W.p2.acs.rds")
death.W.p2.com <- glmmTMB(death_weekly1 ~ com + 
                            scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                            offset(log(pop_denom)) + (1|modzcta), data = weekly.p2,
                          family = poisson, ziformula  = ~ 1)
saveRDS(death.W.p2.com, "weekly.p2/death.W.p2.com.rds")
death.W.p2.acs.2 <- glmmTMB(death_weekly2 ~ acs + 
                              scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                              offset(log(pop_denom)) + (1|modzcta), data = weekly.p2,
                            family = poisson, ziformula  = ~ 1)
saveRDS(death.W.p2.acs.2, "weekly.p2/death.W.p2.acs.2.rds")
death.W.p2.com.2 <- glmmTMB(death_weekly2 ~ com + 
                              scale(male_prop) + scale(age_over_65_prop) + scale(pop_non_hispanic_black_prop) + scale(hispanic_prop) +
                              offset(log(pop_denom)) + (1|modzcta), data = weekly.p2,
                            family = poisson, ziformula  = ~ 1)
saveRDS(death.W.p2.com.2, "weekly.p2/death.W.p2.com.2.rds")



